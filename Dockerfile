FROM ubuntu:22.04

RUN apt-get update -y
RUN apt-get install -y software-properties-common python-properties-common openjdk-8-jdk openjdk-8-jdk-headless build-essential pkg-config wget

RUN mkdir /minecraft-server
WORKDIR /minecraft-server

RUN wget -O minecraft-server-1.20.4.jar https://piston-data.mojang.com/v1/objects/8dd1a28015f51b1803213892b50b7b4fc76e594d/server.jar

CMD ["java", "-Xmx1024M", "-Xms1024M", "-jar", "minecraft-server-1.20.4.jar", "nogui"]
